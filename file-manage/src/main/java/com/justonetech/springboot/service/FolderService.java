package com.justonetech.springboot.service;

import com.justonetech.springboot.entity.Folder;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by admin on 2018/1/12.
 */
public interface FolderService extends CrudRepository<Folder, Long> {
    List<Folder> getFoldersByParentIdAndOwnerId(Long parentId,Long ownerId);

//    @Query("select folder from Folder folder where folder.ownerId = :ownerId")
    Folder getFolderByOwnerIdAndUserRootFolder(Long ownerId,Boolean isUserRootFolder);

    List<Folder> getFolderByFolderNameContainsAndOwnerIdAndParentIdIsNot(String folderName,Long ownerId,Long parentId);
}
