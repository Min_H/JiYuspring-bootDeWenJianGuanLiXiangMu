package com.justonetech.springboot.service;

import com.justonetech.springboot.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by admin on 2018/1/11.
 */

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface UserService extends CrudRepository<User, Long>{
    User findByUsernameOrEmail(String username, String email);

    User findByUsername(String username);
}
