package com.justonetech.springboot.service;

import com.justonetech.springboot.entity.CloudFile;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by admin on 2018/1/12.
 */
public interface CloudFileService extends CrudRepository<CloudFile, Long> {
    List<CloudFile> getCloudFileByFolder_IdAndOwnerId(Long folderId, Long ownerId);

    List<CloudFile> getCloudFileByFileNameContainsAndOwnerId(String fileName, Long Owner);
}
