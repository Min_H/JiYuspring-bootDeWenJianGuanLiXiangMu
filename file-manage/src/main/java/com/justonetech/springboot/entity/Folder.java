package com.justonetech.springboot.entity;


import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by admin on 2018/1/12.
 */
@Entity
public class Folder {

    private Long id;

    private String folderName;
    private Long parentId;
    private Date createDate;
    private Long ownerId;
    private String folderPath;//the tree of folder
    private boolean UserRootFolder;

    private Set<CloudFile> cloudFiles;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    @OneToMany(mappedBy = "folder", cascade = CascadeType.ALL)
    public Set<CloudFile> getCloudFiles() {
        return cloudFiles;
    }

    public void setCloudFiles(Set<CloudFile> cloudFiles) {
        this.cloudFiles = cloudFiles;
    }

    public boolean isUserRootFolder() {
        return UserRootFolder;
    }

    public void setUserRootFolder(boolean userRootFolder) {
        UserRootFolder = userRootFolder;
    }

    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }
}
