package com.justonetech.springboot.entity;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/**
 * Created by admin on 2018/1/12.
 */
@Entity
public class Authorities implements GrantedAuthority {

    private Long Id;
    private String username;
    private String authority;

    @Override
    public String getAuthority() {
        return this.authority;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public Authorities(String username, String authority) {
        this.authority = authority;
        this.username = username;
    }
}
