package com.justonetech.springboot.service;

import com.justonetech.springboot.entity.Authorities;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by admin on 2018/1/12.
 */
public interface AuthoritiesService extends CrudRepository<Authorities, Long> {
}
