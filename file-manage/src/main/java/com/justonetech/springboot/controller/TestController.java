package com.justonetech.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by admin on 2018/1/11.
 */
@Controller
public class TestController {
    @RequestMapping("/")
    public String index(Model model, HttpServletRequest request) {
        String remoteUser = request.getRemoteUser();
        if (remoteUser==null){
            remoteUser = "guest";
        }
        model.addAttribute("username",remoteUser);
        return "index";
    }
}
